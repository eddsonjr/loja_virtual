package com.edsonjr.lojavirtual.Fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.edsonjr.lojavirtual.Adapter.RecyclerViewAdapter
import com.edsonjr.lojavirtual.Model.Dados
import com.edsonjr.lojavirtual.R
import com.google.firebase.firestore.FirebaseFirestore
import java.io.DataOutput


class Produtos : Fragment() {

    private val TAG = "[FragPRODUTOS]:"
    private var listaProdutosFirebase: ArrayList<Dados>? = null
    //private lateinit var recyclerViewAdapter: RecyclerViewAdapter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_produtos, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        buscarProdutosDoBanco()

    }


    private fun buscarProdutosDoBanco() {
        listaProdutosFirebase = ArrayList<Dados>()
        FirebaseFirestore.getInstance().collection("Produtos")
            .addSnapshotListener { snapshot, exception ->
                exception?.let {
                    return@addSnapshotListener
                }

                snapshot?.let {
                    for (doc in snapshot) {
                        val produto = doc.toObject(Dados::class.java)
                        Log.d(TAG, "firebase: $produto")
                        listaProdutosFirebase?.add(produto)
                        Log.d(TAG, "QT. Documents: ${it.size()}")
                    }
                    Log.d(
                        TAG,
                        "QT. Dados baixados: ${listaProdutosFirebase?.size}. Configurando recyclerview"
                    )
                    setupRecyclerView(view)
                }

            }
    }


    private fun setupRecyclerView(view: View?) {
        val context = view?.context
        val recyclerView = view?.findViewById<RecyclerView>(R.id.recycler_produtos)
        recyclerView?.setHasFixedSize(true)
        val recyclerAdapter =  RecyclerViewAdapter(context,listaProdutosFirebase!!)
        recyclerView?.adapter = recyclerAdapter
    }

}