package com.edsonjr.lojavirtual.Adapter

import com.edsonjr.lojavirtual.Model.Dados

interface RecyclerViewClickListener {

    fun clickAction(dados: Dados)
}