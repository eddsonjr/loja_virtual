package com.edsonjr.lojavirtual.Adapter

import android.app.AlertDialog
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.materialdialogs.MaterialDialog
import com.edsonjr.lojavirtual.Model.Dados
import com.edsonjr.lojavirtual.R
import com.google.android.material.dialog.MaterialDialogs
import com.squareup.picasso.Picasso

class RecyclerViewAdapter(val contextView: Context?, val listaProdutos: ArrayList<Dados>):
    RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>(),RecyclerViewClickListener {


    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val fotoProduto = view.findViewById<ImageView>(R.id.foto_produto)
        val nomeProduto = view.findViewById<TextView>(R.id.nome_produto)
        val precoProduto = view.findViewById<TextView>(R.id.preco_produto)


        fun bind(data: Dados) {
            Picasso.get().load(data.uid).into(fotoProduto)
            nomeProduto.text = data.nome
            precoProduto.text = data.preco
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.lista_produtos,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listaProdutos.get(position))

        holder.itemView.setOnClickListener {
            clickAction(listaProdutos.get(position))
        }

    }

    override fun getItemCount(): Int {return listaProdutos.size }




    override fun clickAction(dados: Dados) {
        val dialogView = LayoutInflater.from(contextView).inflate(R.layout.pagamento_dialog,null)
        val builder = AlertDialog.Builder(contextView).setView(dialogView)
            .setTitle("Formas de Pagamento")
        val mAlertDialog = builder.show()
        val confimarPagamentoTXT = mAlertDialog.findViewById<TextView>(R.id.bt_pagar)

        //quando clicar no text de confirmar o pagamento, ira executar a logica de pagamento
        confimarPagamentoTXT.setOnClickListener {
            val pagamento = mAlertDialog.findViewById<EditText>(R.id.fm_pagamento).text.toString().trim()
            Log.d("RECYCLERVIEW","Validando forma de pagamento. Valor pagamento: $pagamento")
            if(pagamento.equals("237,99")){
                MaterialDialog(contextView!!).show {
                    title(text = "Pagamento Concluído!")
                    message(text = "Obrigado pela compra e volte sempre =]")
                }
                mAlertDialog.dismiss()
            }else{
                MaterialDialog(contextView!!).show {
                    title(text = "Pagamento Recusado!")
                    message(text = "Parece que seu pagamento foi recusado. Verifique o valor informado e tente novamente")
                }
                mAlertDialog.dismiss()
            }
        }
    }
}