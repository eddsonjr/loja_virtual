package com.edsonjr.lojavirtual


import android.content.Intent
import android.os.Bundle
import com.edsonjr.lojavirtual.Form.FormLogin
import com.heinrichreimersoftware.materialintro.app.IntroActivity
import com.heinrichreimersoftware.materialintro.slide.SimpleSlide


class Slides : IntroActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_slides) - para trabalhar com a lib de slides e necessario nao carregar o layout.xml

        //esconendo os botoes dos slides
        isButtonBackVisible = false //coloca o botao de voltar escondido
        isButtonNextVisible = false //coloca o botao de avancar escondido



        //Criando os slides - note que para funcionar, e necessario 'invalidar' o layout original do Android e criar o
        addSlide(
            SimpleSlide.Builder()
                .background(R.color.roxo)
                .image(R.drawable.drawer)
                .backgroundDark(R.color.branco) //coloca branco nos botoes e na status bar
                .title("Loja Online de Calçados")
                .description("Entre e confira as promoções que preparamos para você")
                .build()
        )


        addSlide(
            SimpleSlide.Builder()
                .background(R.color.AV)
                .title("Crie uma conta gratis")
                .canGoBackward(false) //impeca de voltar para o primeiro slide
                .description("Cadastra-se agora mesmo! E venha conhecer nossos produtos.")
                .build()
        )
    }

    //sobrescrevendo o metodo onDestroy para finalizar os slides e chamar a activity de login
    override fun onDestroy() {
        super.onDestroy()
        var intent = Intent(this,FormLogin::class.java)
        startActivity(intent)
        finish()
    }
}