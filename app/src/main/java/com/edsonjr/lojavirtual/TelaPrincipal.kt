package com.edsonjr.lojavirtual

import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.widget.Toolbar
import androidx.appcompat.app.ActionBarDrawerToggle
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.navigation.NavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import com.edsonjr.lojavirtual.Form.CadastroProdutosActivity
import com.edsonjr.lojavirtual.Form.FormLogin
import com.edsonjr.lojavirtual.Fragments.Produtos
import com.edsonjr.lojavirtual.databinding.ActivityTelaPrincipalBinding
import com.google.firebase.auth.FirebaseAuth

class TelaPrincipal : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityTelaPrincipalBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityTelaPrincipalBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.appBarTelaPrincipal.toolbar)
        carregarFragmentoProdutos()


        val drawerLayout: DrawerLayout = binding.drawerLayout
        val navView: NavigationView = binding.navView
        val toggle = ActionBarDrawerToggle( //configura o menu lateral para aparecer novamente
            this,drawerLayout,binding.appBarTelaPrincipal.toolbar,R.string.navigation_drawer_open,R.string.navigation_drawer_close
        )

        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        navView.setNavigationItemSelectedListener(this)




    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.tela_principal, menu)
        return true
    }






    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if(id == R.id.action_settings){
            FirebaseAuth.getInstance().signOut()
            voltarParaFormLogin()
        }

        return super.onOptionsItemSelected(item)
    }


    private fun voltarParaFormLogin(){
        val intent = Intent(this,FormLogin::class.java)
        startActivity(intent)
        finish() //impede de voltar para tela quando houve a transicao

    }



    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id = item.itemId //pegando o id dos itens da navigation drawer

        if(id == R.id.nav_produtos){
            carregarFragmentoProdutos()
        }else if(id == R.id.nav_cadastrar_produtos){
            val intent = Intent(this,CadastroProdutosActivity::class.java)
            startActivity(intent)
        }else if(id == R.id.nav_contato){
            enviarEmail()
        }

        val drawer = binding.drawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }


    private fun carregarFragmentoProdutos() {
        val produtosFragment = Produtos()
        val fragment = supportFragmentManager.beginTransaction()
        fragment.replace(R.id.frameContainer,produtosFragment)
        fragment.commit()
    }

    private fun enviarEmail() {
        val PACKAGE_GOOGLE_MAIL = "com.google.android.gm"
        val email = Intent(Intent.ACTION_SEND)
        email.putExtra(Intent.EXTRA_EMAIL, arrayOf(""))
        email.putExtra(Intent.EXTRA_SUBJECT,"") //assunto do email
        email.putExtra(Intent.EXTRA_TEXT,"Loja de Sapatos virtual") //texto do email (corpo do email)

        //configuracoes de apps de envio de email
        email.type = "message/rec822" // vai configurar para abrir aplicativos de envio de email
        email.setPackage(PACKAGE_GOOGLE_MAIL)
        startActivity(Intent.createChooser(email,"Escolha o app de e-mail")) //permite o usuario escolher o app de email, caso tenha mais de um
    }
}