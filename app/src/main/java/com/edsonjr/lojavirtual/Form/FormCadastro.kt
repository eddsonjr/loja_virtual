package com.edsonjr.lojavirtual.Form

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.edsonjr.lojavirtual.R
import com.edsonjr.lojavirtual.databinding.ActivityFormCadastroBinding
import com.edsonjr.lojavirtual.databinding.ActivityFormLoginBinding
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth

class FormCadastro : AppCompatActivity() {

    private lateinit var binding: ActivityFormCadastroBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFormCadastroBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar!!.hide()

        //configurando o click no botao
        binding.botaoCadastrar.setOnClickListener {
            CadastrarUsuario()

        }
    }

    private fun CadastrarUsuario(){
        val email = binding.editEmail.text.toString()
        val senha = binding.editSenha.text.toString()

        if(email.isNullOrEmpty() || senha.isNullOrEmpty()){
          showSnackBar("Informe o seu e-mail e/ou senha")


        }else{
            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,senha).addOnCompleteListener{
                if(it.isSuccessful){

                    //SOmente para demonstracao
                    showSnackBarAndAction("Usuario cadastrado com sucesso",{
                        var intent = Intent(this,FormLogin::class.java)
                        startActivity(intent)
                        finish()
                    })
                }
            }.addOnFailureListener {
                    showSnackBarAndBack("Usuario cadastrado com sucesso")
            }
        }
    }


    //funcao para mostrar a snackbar com uma mensagem customizada
    private fun showSnackBar(msg: String){
        var snackbar = Snackbar.make(binding.layoutCadastro,msg,Snackbar.LENGTH_INDEFINITE)
            .setBackgroundTint(Color.WHITE)
            .setTextColor(Color.BLACK)
            .setAction("Ok", View.OnClickListener {})
        snackbar.show()
    }


    private fun showSnackBarAndBack(msg: String){

        var snackbar = Snackbar.make(binding.layoutCadastro,msg,Snackbar.LENGTH_INDEFINITE)
            .setBackgroundTint(Color.WHITE)
            .setTextColor(Color.BLACK)
            .setAction("Ok", View.OnClickListener {
                VoltarParaFormLogin()
            })
        snackbar.show()
    }



    private fun showSnackBarAndAction(msg: String,action:()->Unit){
        var snackbar = Snackbar.make(binding.layoutCadastro,msg,Snackbar.LENGTH_INDEFINITE)
            .setBackgroundTint(Color.WHITE)
            .setTextColor(Color.BLACK)
            .setAction("Ok", View.OnClickListener {
                action()
            })
        snackbar.show()
    }

    private fun VoltarParaFormLogin(){
        var intent = Intent(this,FormLogin::class.java)
        startActivity(intent)
        finish()
    }


}