package com.edsonjr.lojavirtual.Form

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import com.edsonjr.lojavirtual.R
import com.edsonjr.lojavirtual.TelaPrincipal
import com.edsonjr.lojavirtual.databinding.ActivityFormLoginBinding
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth

class FormLogin : AppCompatActivity() {

    private lateinit var binding: ActivityFormLoginBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFormLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar!!.hide() //escondendo da toolbar

        VerificarUsuarioLogado() //verificando se o usuario esta logado

        //adicionando o evento de click no textView para chamar a tela de cadastro
        binding.textTelaCadastro.setOnClickListener {
            val intent = Intent(this,FormCadastro::class.java)
            startActivity(intent)

        }

        binding.botaoEntrar.setOnClickListener {
            AutenticarUsuario()
        }

    }
    private fun AutenticarUsuario(){
        val email = binding.editEmail.text.toString()
        val senha = binding.editSenha.text.toString()

        if(email.isNullOrEmpty() || senha.isNullOrEmpty()){
            var snackbar = Snackbar.make(binding.layoutLogin,"Informe o email e a senha para prosseguir",Snackbar.LENGTH_INDEFINITE)
                .setBackgroundTint(Color.WHITE).setTextColor(Color.BLACK)
                .setAction("OK", View.OnClickListener {

                })
            snackbar.show()
        }else{
            FirebaseAuth.getInstance().signInWithEmailAndPassword(email,senha).addOnCompleteListener {
                if(it.isSuccessful){
                    binding.frameL.visibility = View.VISIBLE
                    Handler().postDelayed({
                        AbrirTelaPrincipal()
                    },3000)

                } //trabalhando com o framelayout para criar uma tela de carregamento


            }.addOnFailureListener {
                var snackbar = Snackbar.make(binding.layoutLogin,"Erro ao logar usuário!",Snackbar.LENGTH_INDEFINITE)
                    .setBackgroundTint(Color.WHITE).setTextColor(Color.BLACK)
                    .setAction("OK", View.OnClickListener {

                    })
                snackbar.show()
            }
        }
    }

    private fun AbrirTelaPrincipal() {
        var intent = Intent(this,TelaPrincipal::class.java)
        startActivity(intent)
        finish()
    }



    private fun VerificarUsuarioLogado() {
        val usuarioAtual = FirebaseAuth.getInstance().currentUser
        if(usuarioAtual != null){
            AbrirTelaPrincipal()
        }

    }
}