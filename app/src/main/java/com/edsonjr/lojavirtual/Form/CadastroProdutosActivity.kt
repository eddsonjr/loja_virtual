package com.edsonjr.lojavirtual.Form

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.widget.Toast
import com.edsonjr.lojavirtual.Model.Dados
import com.edsonjr.lojavirtual.R
import com.edsonjr.lojavirtual.databinding.ActivityCadastroProdutosBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import java.util.*

class CadastroProdutosActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCadastroProdutosBinding
    private var selecionarUri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCadastroProdutosBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btSelecionarFoto.setOnClickListener {
            selecionarFotoGaleria()
        }

        binding.btSalvarProduto.setOnClickListener{
            salvarDadosNoFirebase()
        }

    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == 0){
            selecionarUri = data?.data //pegando os dados da intent
            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver,selecionarUri)
            binding.fotoProduto.setImageBitmap(bitmap)
            binding.btSelecionarFoto.alpha = 0f
        }
    }




    private fun selecionarFotoGaleria(){
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent,0)
    }


    private fun salvarDadosNoFirebase(){
        val nomeArquivo = UUID.randomUUID().toString()
        val referencia = FirebaseStorage.getInstance().getReference(
            "/imagens/${nomeArquivo}"
        )

        selecionarUri?.let {
            referencia.putFile(it)
                .addOnSuccessListener {
                    referencia.downloadUrl.addOnSuccessListener {
                            val url = it.toString()
                            val nomeProduto = binding.editNomeProduto.text.toString()
                            val precoProduto = binding.editPreco.text.toString()
                            val uid = FirebaseAuth.getInstance().uid

                            val produtos = Dados(url,nomeProduto,precoProduto)

                            FirebaseFirestore.getInstance().collection("Produtos")
                                .add(produtos).addOnSuccessListener {
                                    Toast.makeText(this,"Produto cadastrado com sucesso",Toast.LENGTH_SHORT).show()
                                }.addOnFailureListener {
                                    Toast.makeText(this,"Nao foi possível cadastrar o produto",Toast.LENGTH_SHORT).show()
                                }
                    }
                }
        }

    }
}